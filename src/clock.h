#ifndef _CLOCK_H_
#define _CLOCK_H_

#include <inttypes.h>

void CLOCK_newSecond(void);
uint8_t CLOCK_getHour(void);
uint8_t CLOCK_getMinute(void);
void CLOCK_incrementHours(void);
void CLOCK_incrementMinutes(void);
void CLOCK_storeTime(void);
#endif
