#ifndef _DISPLAY_H
#define _DISPLAY_H

#include <inttypes.h>

void DISPLAY_init(void);
void DISPLAY_toggleDots(void);
void DISPLAY_setNumber(uint32_t number);
void DISPLAY_setDigit(uint8_t digit, uint8_t position);

#endif
