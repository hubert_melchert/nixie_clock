#include "display.h"

#include "stm32f10x.h"
#include "STM32F1_gpio.h"
#include <stddef.h>


#define NELEMS(a) (sizeof(a) / sizeof(a[0]))


typedef struct
{
    GPIO_TypeDef *port;
    uint8_t pin;
}pin_t;

typedef enum
{
    BCD_A = 0,
    BCD_B,
    BCD_C,
    BCD_D
}BCD_t;

typedef pin_t lamp_t[4];

//<pinout>
static lamp_t lamps[4] = {
          [0] = {[BCD_A] = {.port = GPIOB, .pin = 1},
                 [BCD_B] = {.port = GPIOB, .pin = 0},
                 [BCD_C] = {.port = GPIOC, .pin = 5},
                 [BCD_D] = {.port = GPIOC, .pin = 4}
          },
          [1] = {[BCD_A] = {.port = GPIOA, .pin = 7},
                 [BCD_B] = {.port = GPIOA, .pin = 6},
                 [BCD_C] = {.port = GPIOA, .pin = 5},
                 [BCD_D] = {.port = GPIOA, .pin = 4}
          },
          [2] = {[BCD_A] = {.port = GPIOB, .pin = 12},
                 [BCD_B] = {.port = GPIOB, .pin = 13},
                 [BCD_C] = {.port = GPIOB, .pin = 14},
                 [BCD_D] = {.port = GPIOB, .pin = 15}
          },
          [3] = {[BCD_A] = {.port = GPIOC, .pin = 6},
                 [BCD_B] = {.port = GPIOC, .pin = 7},
                 [BCD_C] = {.port = GPIOC, .pin = 8},
                 [BCD_D] = {.port = GPIOC, .pin = 9}
          }
};

static pin_t dots = {.port = GPIOA, .pin = 8};
//</pinout>

void DISPLAY_init(void)
{
    RCC->APB2ENR |= RCC_APB2Periph_GPIOA
                    | RCC_APB2Periph_GPIOC
                    | RCC_APB2Periph_GPIOB;

    for(size_t i = 0; i < NELEMS(lamps); i++)
    {
        pin_t *lamp = &(*(&lamps[i])[0]);
        GPIO_setOutputMode(lamp[BCD_A].port, GPIO_outputMode_GeneralPP, GPIO_outputSpeed_2MHz, lamp[BCD_A].pin);
        GPIO_setOutputMode(lamp[BCD_B].port, GPIO_outputMode_GeneralPP, GPIO_outputSpeed_2MHz, lamp[BCD_B].pin);
        GPIO_setOutputMode(lamp[BCD_C].port, GPIO_outputMode_GeneralPP, GPIO_outputSpeed_2MHz, lamp[BCD_C].pin);
        GPIO_setOutputMode(lamp[BCD_D].port, GPIO_outputMode_GeneralPP, GPIO_outputSpeed_2MHz, lamp[BCD_D].pin);
        GPIO_setOut(lamp[BCD_A].port, lamp[BCD_A].pin);
        GPIO_setOut(lamp[BCD_B].port, lamp[BCD_B].pin);
        GPIO_setOut(lamp[BCD_C].port, lamp[BCD_C].pin);
        GPIO_setOut(lamp[BCD_D].port, lamp[BCD_D].pin);
    }

    GPIO_setOutputMode(dots.port, GPIO_outputMode_GeneralPP, GPIO_outputSpeed_2MHz, dots.pin);
}

void DISPLAY_toggleDots(void)
{
    dots.port->ODR ^= (1 << dots.pin);
}

void DISPLAY_setNumber(uint32_t number)
{
    for(size_t d = 0; d < NELEMS(lamps); d++)
    {
        DISPLAY_setDigit(number % 10, NELEMS(lamps) - d -1);
        number /= 10;
    }
}


void DISPLAY_setDigit(uint8_t digit, uint8_t position)
{
    const uint8_t digitsMap[] = {7, 5, 9, 1, 0, 8, 4, 6, 2, 3};
    if(digit >= NELEMS(digitsMap)
       || position >= NELEMS(lamps))
    {
        return;
    }

    digit = digitsMap[digit];

    pin_t *lamp = &(*(&lamps[position])[0]);
    for(size_t p = 0; p < NELEMS(lamps[0]); p++)
    {
        if(digit & (1 << p))
        {
            GPIO_setOut(lamp[p].port, lamp[p].pin);
        }
        else
        {
            GPIO_clearOut(lamp[p].port, lamp[p].pin);
        }
    }
}
