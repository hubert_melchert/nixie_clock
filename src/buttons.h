#ifndef _BUTTONS_H_
#define _BUTTONS_H_
#include <stdint.h>
#include <stdbool.h>

void BUTTONS_init(void);
void BUTTON_handler(void);
bool BUTTON_clicked(uint8_t num);
bool BUTTON_hold(uint8_t num1, uint8_t num2);

#endif

