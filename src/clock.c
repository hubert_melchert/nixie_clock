#include "clock.h"
#include "RTC/rtc.h"

static uint8_t hours = 0;
static uint8_t minutes = 0;

#define SECONDS_IN_DAY (60 * 60 * 24)
#define SECONDS_IN_HOUR (60 * 60)

void CLOCK_newSecond(void)
{
    uint32_t timestamp = RTC_getTimestamp();    
    
    timestamp = timestamp % SECONDS_IN_DAY;

    hours = timestamp / SECONDS_IN_HOUR;
    timestamp -= (hours * SECONDS_IN_HOUR);
    minutes = timestamp / 60;

//    ++seconds;
//    if(seconds == 60)
//    {
//        seconds = 0;
//        ++minutes;
//        if(minutes == 60)
//        {
//            minutes = 0;
//            ++hours;
//            if(hours == 24)
//            {
//                hours = 0;
//            }
//        }
//    }
}

uint8_t CLOCK_getHour(void)
{
    return hours;
}

uint8_t CLOCK_getMinute(void)
{
    return minutes;
}

void CLOCK_incrementMinutes(void)
{
    ++minutes;
    if(minutes == 60)
    {
        minutes = 0;
    }
}

void CLOCK_incrementHours(void)
{
    ++hours;
    if(hours == 24)
    {
        hours = 0;
    }
}

void CLOCK_storeTime(void)
{
    RTC_setTimestamp(hours * SECONDS_IN_HOUR + minutes * 60);
}
