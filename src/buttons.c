#include "buttons.h"
#include "stm32f10x.h"
#include "STM32F1_gpio.h"
#include <stddef.h>

#define NELEMS(a) (sizeof(a) / sizeof(a[0]))

typedef struct
{
    GPIO_TypeDef *port;
    uint8_t pin;
}pin_t;

typedef struct
{
    pin_t pin;
    uint16_t counter;
    bool clicked;
    bool hold;
}button_t;

static button_t buttons[] = {
        [0] = {.pin = {.port = GPIOA, .pin = 15}},
        [1] = {.pin = {.port = GPIOC, .pin = 10}},
        [2] = {.pin = {.port = GPIOC, .pin = 11}},
        [3] = {.pin = {.port = GPIOC, .pin = 12}}
};

void BUTTONS_init(void)
{
    
    RCC->APB2ENR |= RCC_APB2Periph_GPIOA
                    | RCC_APB2Periph_GPIOC
                    | RCC_APB2Periph_GPIOB;

    for(size_t i = 0; i < NELEMS(buttons); i++)
    {
        const pin_t *pin = &(buttons[i].pin);
        GPIO_setInputMode(pin->port, GPIO_inputMode_Pull, pin->pin, 1);
    }
}

void BUTTON_handler(void)
{
    for(size_t i = 0; i < NELEMS(buttons); i++)
    {
        button_t *button = &buttons[i];
        const pin_t *pin = &(buttons[i].pin);

        if(GPIO_getInput(pin->port, pin->pin))
        {
            if(button->counter < 1000)
            {
                ++(button->counter);
                if(button->counter == 1000)
                {
                    button->hold = true;
                }
            }
        }
        else
        {
            if(button->counter > 100 && button->counter < 500)
            {
                button->clicked = true;
            }
            button->counter = 0;
        }
    }
}

bool BUTTON_clicked(uint8_t num)
{
    if(num >= NELEMS(buttons))
    {
        return false;
    }

    button_t *button = &buttons[num];
    if(button->clicked)
    {
        button->clicked = false;
        return true;
    }
    return false;
}

bool BUTTON_hold(uint8_t num1, uint8_t num2)
{
    if(num1 >= NELEMS(buttons)
       || num2 >= NELEMS(buttons))
    {
        return false;
    }

    button_t *button1 = &buttons[num1];
    button_t *button2 = &buttons[num2];

    if(button1->hold && button2->hold)
    {
        button1->hold = false;
        button2->hold = false;
        return true;
    }
    return false;
}
