#ifndef _RTC_H_
#define _RTC_H_
#include <stdbool.h>
#include <stdint.h>

void RTC_init(void);
bool RTC_isNewSecond(void);
uint8_t RTC_setTimestamp(uint32_t timestamp);
uint32_t RTC_getTimestamp(void);
#endif
