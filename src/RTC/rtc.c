#include <inttypes.h>
#include "rtc.h"
#include "stm32f10x.h"

#define BIT_MASK(b) ((1 << b) - 1)
static void waitForLastWriteEnd(void);
static void setPrescaler(uint32_t prescaler);
static void enterConfigMode(void);
static void exitConfigMode(void);
static volatile bool newSecond = false;

void RTC_updateBackup(uint8_t hours, uint8_t minutes, uint8_t seconds)
{

}
void RTC_init(void)
{
#define RESET_BKP 0
#if RESET_BKP
    RCC->BDCR |= RCC_BDCR_BDRST;
    RCC->BDCR &= ~RCC_BDCR_BDRST;
#endif
    RCC->APB1ENR |= RCC_APB1ENR_BKPEN | RCC_APB1ENR_PWREN;
    PWR->CR |= PWR_CR_DBP;

    if(!(RCC->BDCR & RCC_BDCR_LSERDY))
    {
        RCC->BDCR |= RCC_BDCR_LSEON;
        uint32_t timeout = 0xFFFFFF;
        while(!(RCC->BDCR & RCC_BDCR_LSERDY))
        {
            timeout--;
            if(timeout == 0)
            {
                return;
            }
        }
        waitForLastWriteEnd();
        setPrescaler(32768);
    }
    if((RCC->BDCR & (RCC_BDCR_RTCEN | RCC_BDCR_RTCSEL))
       != (RCC_BDCR_RTCEN | RCC_BDCR_RTCSEL_LSE))
    {
        RCC->BDCR |= RCC_BDCR_RTCEN | RCC_BDCR_RTCSEL_LSE;
    }

    NVIC_EnableIRQ(RTC_IRQn);
    RTC->CRH |= RTC_CRH_SECIE;
}

void RTC_IRQHandler(void)
{
    RTC->CRL &= ~RTC_CRL_SECF;
    newSecond = true;
}

bool RTC_isNewSecond(void)
{
    if(newSecond)
    {
        newSecond = false;
        return true;
    }
    return false;
}

uint8_t RTC_setTimestamp(uint32_t timestamp)
{
    enterConfigMode();
    RTC->CNTH = timestamp >> 16;
    RTC->CNTL = timestamp & BIT_MASK(16);
    exitConfigMode();
    return 1;
}

uint32_t RTC_getTimestamp(void)
{
    return (RTC->CNTH << 16) | RTC->CNTL;
}

static void waitForLastWriteEnd(void)
{
    while(!(RTC->CRL & RTC_CRL_RTOFF));
}

static void setPrescaler(uint32_t prescaler)
{
    prescaler--;
    enterConfigMode();
    RTC->PRLL = prescaler & BIT_MASK(16);
    RTC->PRLH = (prescaler >> 16);
    exitConfigMode();
}

static void enterConfigMode(void)
{
    RTC->CRL |= RTC_CRL_CNF;
}

static void exitConfigMode(void)
{
    RTC->CRL &= (uint16_t) ~ ((uint16_t)RTC_CRL_CNF);
}


