#include <stddef.h>
#include "stm32f10x.h"
#include "DISPLAY/display.h"
#include "RTC/rtc.h"
#include "buttons.h"
#include "clock.h"


static volatile uint32_t delay = 0;
static volatile bool checkButtons = false;

static void updateDisplayWithTime(void);
void delayMs(uint32_t ms)
{
    delay = ms;
    while(delay);
}

void SysTick_Handler(void)
{
    --delay;
    checkButtons = true;
}

int main(void)
{
    SystemCoreClockUpdate();
    SysTick_Config(SystemCoreClock / 1000);
    DISPLAY_init();
    BUTTONS_init();
    for(size_t i = 0; i < 20; i++)
    {
        delayMs(500);
        DISPLAY_toggleDots();
    }
    RTC_init();

    static uint8_t points[4] = {0};

    while(1)
    {
        static enum
        {
            Mode_Clock,
            Mode_SetClock,
            Mode_Points
        }mode = Mode_Clock;

        if(checkButtons)
        {
            checkButtons = false;
            BUTTON_handler();
        }

        if(BUTTON_hold(0, 3))
        {
            if(mode == Mode_Clock)
            {
                mode = Mode_Points;
                for(uint8_t i = 0; i < 4; i++)
                {
                    DISPLAY_setDigit(points[i], 3 - i);
                }
            }
            else if(mode == Mode_Points)
            {
                mode = Mode_Clock;
            }
        }
        if(BUTTON_hold(1, 2))
        {
            if(mode == Mode_Clock)
            {
                mode = Mode_SetClock;
            }
            else if(mode == Mode_SetClock)
            {
                mode = Mode_Clock;
                CLOCK_storeTime();
                CLOCK_newSecond();
                updateDisplayWithTime();
            }
        }

        if(mode == Mode_Points)
        {
            if(RTC_isNewSecond())
            {
                CLOCK_newSecond();
            }
            for(uint8_t i = 0; i < 4; i++)
            {
                if(BUTTON_clicked(i))
                {
                    ++points[i];
                    if(points[i] == 10)
                    {
                        points[i] = 0;
                    }
                    DISPLAY_setDigit(points[i], 3 - i);
                }
            }
        }
        else if(mode == Mode_Clock)
        {
            if(RTC_isNewSecond())
            {
                CLOCK_newSecond();
                updateDisplayWithTime();

                DISPLAY_toggleDots();
            }
        }
        else if(mode == Mode_SetClock)
        {
            if(BUTTON_clicked(0))
            {
                CLOCK_incrementMinutes();
                updateDisplayWithTime();
            }
            if(BUTTON_clicked(3))
            {
                CLOCK_incrementHours();
                updateDisplayWithTime();
            }
        }
    }
}

static void updateDisplayWithTime(void)
{
    uint8_t hours = CLOCK_getHour();
    uint8_t minutes = CLOCK_getMinute();

    DISPLAY_setDigit(hours / 10, 0);
    DISPLAY_setDigit(hours % 10, 1);

    DISPLAY_setDigit(minutes / 10, 2);
    DISPLAY_setDigit(minutes % 10, 3);
}
