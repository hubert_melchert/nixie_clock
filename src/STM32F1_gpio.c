#include "STM32F1_gpio.h"

#ifndef MASK
#define MASK(bits) ((1 << bits) - 1)
#endif

void GPIO_setOutputMode(GPIO_TypeDef *port, GPIO_outputMode_t mode, GPIO_outputSpeed_t speed, uint16_t pin)
{
    volatile uint32_t *ctrlReg = pin > 7? &port->CRH : &port->CRL;
    pin %= 8;
    *ctrlReg &= ~(MASK(4) << (pin * 4)); //MODEy = CNFy = 0
    *ctrlReg |= ((uint8_t)speed << (pin * 4));
    *ctrlReg |= ((uint8_t)mode << (pin * 4 + 2));
}

void GPIO_setInputMode(GPIO_TypeDef *port, GPIO_inputMode_t mode, uint16_t pin, uint8_t pull)
{
    volatile uint32_t *ctrlReg = pin > 7? &port->CRH : &port->CRL;
    *ctrlReg &= ~(MASK(4) << ((pin % 8) * 4)); //MODEy = CNFy = 0
    *ctrlReg |= (mode << ((pin % 8) * 4 + 2));
    if(mode == GPIO_inputMode_Pull)
    {
        if(pull)
        {
            port->ODR |= (1 << pin);
        }
        else
        {
            port->ODR &= ~(1 << pin);
        }
    }
}

uint8_t GPIO_getInput(GPIO_TypeDef *port, uint16_t pin)
{
    return ((port->IDR & (1 << pin)) > 0 ? 0x01: 0x00);
}
