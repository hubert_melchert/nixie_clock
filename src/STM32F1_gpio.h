#ifndef STM32F1_GPIO_H_
#define STM32F1_GPIO_H_

#include "stm32f10x.h"
typedef enum __attribute__((packed))
{
    GPIO_outputMode_GeneralPP   = 0x00,
    GPIO_outputMode_GeneralOD   = 0x01,
    GPIO_outputMode_AlternatePP = 0x02,
    GPIO_outputMode_AlternateOD = 0x03
}GPIO_outputMode_t;

typedef enum __attribute__((packed))
{
    GPIO_inputMode_Analog   = 0x00,
    GPIO_inputMode_Floating = 0x01,
    GPIO_inputMode_Pull     = 0x02,
}GPIO_inputMode_t;

typedef enum __attribute__((packed))
{
    GPIO_outputSpeed_2MHz  = 0x02,
    GPIO_outputSpeed_10MHz = 0x01,
    GPIO_outputSpeed_50MHz = 0x03
}GPIO_outputSpeed_t;
static inline void GPIO_setOut(GPIO_TypeDef *port, uint8_t pin)
{
    port->BSRR = (1 << pin);
}

static inline void GPIO_clearOut(GPIO_TypeDef *port, uint8_t pin)
{
    port->BRR = (1 << pin);
}

void GPIO_setOutputMode(GPIO_TypeDef *port, GPIO_outputMode_t mode, GPIO_outputSpeed_t speed, uint16_t pin);
void GPIO_setInputMode(GPIO_TypeDef *port, GPIO_inputMode_t mode, uint16_t pin, uint8_t pull);
uint8_t GPIO_getInput(GPIO_TypeDef *port, uint16_t pin);
#endif
